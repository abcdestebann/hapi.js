'use strict'

const bcrypt = require('bcrypt')

class User {

   /**
    *Creates an instance of User.
    * @param {*} database
    * @memberof User
    */
   constructor(database) {
      this.database = database
      this.ref = this.database.ref('/')
      this.collection = this.ref.child('users')
   }

   /**
    *
    *
    * @static
    * @param {*} password
    * @returns
    * @memberof User
    */
   static async encrypt(password) {
      const saltRounds = 10
      const hashPassword = await bcrypt.hash(password, saltRounds)
      return hashPassword
   }

   /**
    *
    *
    * @param {*} data
    * @returns
    * @memberof User
    */
   async create(data) {
      data.password = await this.constructor.encrypt(data.password)
      const user = this.collection.push()
      user.set(data)
      return user.key
   }

   /**
    *
    *
    * @param {*} data
    * @returns
    * @memberof User
    */
   async validateUser(data) {
      const userQuery = await this.collection.orderByChild('email').equalTo(data.email).once('value');
      const userFound = userQuery.val()
      if (userFound) {
         const userId = Object.keys(userFound)[0]
         const passwordRight = await bcrypt.compare(data.password, userFound[userId].password)
         const result = (passwordRight) ? userFound[userId] : false
         return result
      }
      return false
   }

}

module.exports = User