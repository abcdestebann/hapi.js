'use strict'
const firebase = require('firebase-admin');
const serviceAccount = require('../config/hapi-course-firebase.json')

firebase.initializeApp({
   credential: firebase.credential.cert(serviceAccount),
   databaseURL: 'https://hapi-course.firebaseio.com'
})

const database = firebase.database()
const User = require('./user')

module.exports = {
   user: new User(database)
}
