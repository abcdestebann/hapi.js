"use strict";
const joi = require("joi");
const sites = require("./controllers/site");
const user = require("./controllers/user");

// ARRAY OF ROUTES
module.exports = [
  {
    method: "GET",
    path: "/home",
    handler: sites.home
  },
  {
    method: "GET",
    path: "/register",
    handler: sites.register
  },
  {
    method: "GET",
    path: "/login",
    handler: sites.login
  },
  {
    path: "/createUser",
    method: "POST",
    options: {
      validate: {
        payload: {
          name: joi.string().min(3).required(), // Validacion de datos
          email: joi.string().email().required(),
          password: joi.string().min(6).required(),
        },
        failAction: user.failValidation
      }
    },
    handler: user.createUser
  },
  {
    path: "/validateUser",
    method: "POST",
    options: {
      validate: {
        payload: {
          email: joi.string().email().required(),
          password: joi.string().min(6).required(),
        },
        failAction: user.failValidation
      }
    },
    handler: user.validateUser
  },
  {
    path: "/logout",
    method: "GET",
    handler: user.logoutUser
  },
  {
    method: "GET",
    path: "/{param*}",
    handler: {
      directory: {
        path: ".",
        index: ["index.html"]
      }
    }
  }];
