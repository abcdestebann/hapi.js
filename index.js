"use strict";

const hapi = require("hapi");
const handlebars = require("handlebars");
const inert = require("inert");
const path = require("path");
const vision = require("vision");
const routes = require("./routes");

// CREATE SERVER
const server = hapi.Server({
  port: process.env.PORT || 3000,
  host: "localhost",
  routes: {
    files: {
      relativeTo: path.join(__dirname, "public")
    }
  }
});

async function start() {
  try {
    await server.register(inert);
    await server.register(vision);

    server.state('user', {
      ttl: 1000 * 60 * 60 * 24 * 7,
      isSecure: process.env.NODE_ENV === 'prod',
      encoding: 'base64json'
    })

    server.views({
      engines: {
        // --- hapi puede usar diferentes engines
        hbs: handlebars // --- asociamos el plugin al tipo de archivos
      },
      relativeTo: __dirname, // --- para que las vistas las busque fuera de /public
      path: "views", // --- directorio donde colocaremos las vistas dentro de nuestro proyecto
      layout: true, // --- indica que usaremos layouts
      layoutPath: "views" // --- ubicación de los layouts
    });

    server.route(routes);

    await server.start();
  } catch (err) {
    console.log(err);
    process.exit(1);
  }
  console.log("Server listening at:", server.info.uri);
}

process.on('unhandledRejection', error => {
  console.error('unhandledRejection', error.message, error)
})

process.on('uncaughtException', error => {
  console.error('uncaughtException', error.message, error)
})

start();
