'use strict'

/**
 *
 *
 * @param {*} req
 * @param {*} h
 * @returns
 */
function home(req, h) {
   return h.view('index', {
      title: 'Home',
      user: req.state.user
   }) // Renderizar vista de handlebars
}

/**
 *
 *
 * @param {*} req
 * @param {*} h
 * @returns
 */
function register(req, h) {
   if (req.state.user) return h.redirect('/home')
   return h.view('register', {
      title: 'Registro',
      user: req.state.user
   })
}

/**
 *
 *
 * @param {*} req
 * @param {*} h
 * @returns
 */
function login(req, h) {
   if (req.state.user) return h.redirect('/home')
   return h.view('login', {
      title: 'Ingresar',
      user: req.state.user
   })
}

module.exports = {
   home,
   register,
   login
}