'use strict'

const User = require('../models/index').user
const boom = require('boom')

/**
 *
 *
 * @param {*} req
 * @param {*} h
 * @returns
 */
async function createUser(req, h) {
   try {
      const result = await User.create(req.payload)
      return h.response(`User created Id: ${result}`)
   } catch (error) {
      console.log(error)
      return h.response('Ocurred problem in create user').code(500)
   }
}

/**
 *
 *
 * @param {*} req
 * @param {*} h
 * @returns
 */
async function validateUser(req, h) {
   try {
      const result = await User.validateUser(req.payload)
      console.log(result)
      if (result) return h.redirect('/home').state('user', { name: result.name, email: result.email })
      else return h.response(`Email or password incorrect`).code(401)

   } catch (error) {
      console.log(error)
      return h.response('Ocurred problem in validate user').code(500)
   }
}

/**
 *
 *
 * @param {*} req
 * @param {*} h
 */
function logoutUser(req, h) {
   return h.redirect('/login').unstate('user')
}

/**
 *
 *
 * @param {*} req
 * @param {*} h
 */
function failValidation(req, h, error) {
   return boom.badRequest('Failed Validation', req.payload)
}


module.exports = {
   createUser,
   validateUser,
   failValidation,
   logoutUser
}